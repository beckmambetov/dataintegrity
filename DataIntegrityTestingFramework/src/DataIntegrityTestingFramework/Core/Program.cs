﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static DataIntegrityTestingFramework.ConfigClass;

namespace DataIntegrityTestingFramework
{
    public class Program
    {
        public static dbConfig dbConfig;

        public static void Main(string[] args)
        {
            string text = System.IO.File.ReadAllText("DbConfig.json");
            dbConfig = JsonConvert.DeserializeObject<dbConfig>(text);
            MongoConnector mg = new MongoConnector(dbConfig.mongoLMS.connectionString);
            mg.getCollection(mg.getDatabase("lms"), "users");
        }
    }
}
