﻿using DataIntegrityTestingFramework.Core.Tools;
using DataIntegrityTestingFramework.dbcontext;
using DataIntegrityTestingFramework.Tools;
using DataIntegrityTestingFramework.Tools.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static DataIntegrityTestingFramework.Core.ConfigurationReader;

namespace DataIntegrityTestingFramework.Core.Common.userBio
{
    public class checkUserBio
    {
        public static List<User> usersFromMongo = new List<User>();

        public void checkUserBioWithDB()
        {

            try
            {
                /* MongoConnector mg = new MongoConnector(dbConfig.mongoLMS.connectionString);
                 IMongoCollection<Object> collection = mg.getCollection(mg.getDatabase(dbConfig.mongoLMS.dbName), "users");
                 List<Object> results = collection.Find("{}").ToList();
                 List<Object> OddResults = new List<object>();
                 for (int i = 0; i < results.Count; i += 2)
                 {
                     OddResults.Add(results[i]);
                     results.RemoveAt(i);
                 }
                 Thread firstThread = new Thread(new ParameterizedThreadStart(Deserialize));
                 // Start the thread
                 firstThread.Start(results);
                 Thread secondThread = new Thread(new ParameterizedThreadStart(Deserialize));
                 // Start the thread
                 secondThread.Start(OddResults);
                 while (firstThread.IsAlive || secondThread.IsAlive)
                 {
                 }
                 User[] temp = new User[usersFromMongo.Count];
                 usersFromMongo.CopyTo(temp);*/
                List<User> usersFromLMS = new List<User>();  
                MongoConnector mg = new MongoConnector(dbConfig.mongoLMS.connectionString);
                IMongoCollection<Object> collection = mg.getCollection(mg.getDatabase(ConfigurationReader.dbConfig.mongoLMS.dbName), "users");
                foreach (Object obj in collection.Find("{}").ToList())
                {

                    User temp = JsonConvert.DeserializeObject<User>(obj.ToJson().Replace("ISODate(", "").Replace(")", "").Replace("ObjectId(", ""));
                    usersFromLMS.Add(temp);
                }
                    
                usersFromMongo.Clear();
                List<LocalUsers> authUsers = getPostgreTransactions();
                authUsers = authUsers.OrderBy(o => o.Id).ToList();
                usersFromLMS = usersFromLMS.OrderBy(o => o._id).ToList();
                Tools.Logger.WriteLog("\n------------------- USER BIO CHECK (LMS with AUTH Postgres)------------------------------");
                int missingCounter = 0, failCounter = 0, successCounter = 0;
                foreach (LocalUsers x in authUsers)
                {
                    if (!usersFromLMS.Any(p => p._id == x.Id.ToString()))
                    {
                        Tools.Logger.WriteLog("NOT IMPORTANT.User with id " + x.Id + " is missing in LMS.(Looks like users without activity on LMS)");
                        missingCounter++;
                        continue;
                    }
                    if (!usersFromLMS.Contains(x.ToUser()))
                    {
                        Tools.Logger.WriteLog("IMPORTANT!!! User with id " + x.Id + " is not the same in both DB");
                        failCounter++;
                        continue;
                    }
                    successCounter++;
                }
              
                Tools.Logger.WriteLog("------------------------ TOTAL: " + authUsers.Count + " PASSED: " + successCounter + " NOT FOUND: " + missingCounter + " NOT EQUALS: " + failCounter);

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void checkUserFromElastic()
        {
            Hits allElasticTransactions = new ElasticSearchConnector().getAllCourses();
            List<LocalUsers> authUsers = getPostgreTransactions();
            List<LocalUsers> creatorsList = new List<LocalUsers>();
            authUsers = authUsers.OrderBy(o => o.Id).ToList();
         
            Tools.Logger.WriteLog("\n------------------- USER BIO ELASTIC CHECK (Elastic with AUTH BIO)   INSTRUCTORS  ------------------------------");
            int passINSTRUCTORS = 0;
            int missmatchINSTRUCTORS = 0;
            int failINSTRUCTORS = 0;
            foreach (Hit x in allElasticTransactions.hits)
            {
                if (x._source.instructors != null)
                {
                    if (x._source.instructors.Count == 0) {
                        Tools.Logger.WriteLog("Instructor in HIT is not found. Count = 0 . Hit id  " + x._id);
                        failINSTRUCTORS++;
                    }
                    foreach (Instructor inst in x._source.instructors)
                    {
                        try
                        {
                            LocalUsers user = authUsers.SingleOrDefault(p => p.Id == int.Parse(inst.original_id));
                            if (inst.email.Equals(user.Email) && inst.first_name.Equals(user.FirstName) && inst.last_name.Equals(user.LastName))
                            {
                                passINSTRUCTORS++;
                            }
                            else
                            {
                                Tools.Logger.WriteLog("Instructor with id " + inst.original_id + " has differences between Elastic and AUTH.");
                                missmatchINSTRUCTORS++;
                            }
                        }
                        catch (Exception e)
                        {
                            Tools.Logger.WriteLog("Instructor with id " + inst.original_id + " is missing in AUTH.");
                            failINSTRUCTORS++;
                        }
                    }
                }
                else {
                    Tools.Logger.WriteLog("Instructor is NULL. Hit id " + x._id);
                    failINSTRUCTORS++;

                }

            }
            Tools.Logger.WriteLog("------------------------ TOTAL: " + allElasticTransactions.hits.Count + " PASSED: " + passINSTRUCTORS + " NOT FOUND: " + failINSTRUCTORS + " NOT EQUALS: " + missmatchINSTRUCTORS);

            Tools.Logger.WriteLog("\n------------------- USER BIO ELASTIC CHECK (Elastic with AUTH BIO)   PUBLISHERS  ------------------------------");
            int passPublishers = 0;
            int missmatchPublishers = 0;
            int failPublishers = 0;
            foreach (Hit x in allElasticTransactions.hits) {
                if (x._source.publisher != null)
                {
                    try
                    {
                        LocalUsers user = authUsers.SingleOrDefault(p => p.Id == int.Parse(x._source.publisher.original_id));
                        if (user.Email.Equals(x._source.publisher.email) && user.FirstName.Equals(x._source.publisher.first_name) && user.LastName.Equals(x._source.publisher.last_name))
                        {
                            passPublishers++;
                        }
                        else
                        {
                            Tools.Logger.WriteLog("Publisher with id " + x._source.publisher.original_id + " has differences between Elastic and AUTH.");
                            missmatchPublishers++;
                        }
                    }
                    catch (Exception e)
                    {
                        Tools.Logger.WriteLog("Instructor with id " + x._source.publisher.original_id + " is missing in AUTH.");
                        failPublishers++;
                    }
                }
                else {
                    Tools.Logger.WriteLog("Publisher is NULL. Hit id " + x._id);
                    failPublishers++;
                }
            }

            Tools.Logger.WriteLog("------------------------ TOTAL: " + allElasticTransactions.hits.Count + " PASSED: " + passPublishers + " NOT FOUND: " + failPublishers + " NOT EQUALS: " + missmatchPublishers);
           
        }

        public void Deserialize(object results)
        {
            List<Object> listOfResults = (List<Object>)results;
            foreach (Object obj in listOfResults)
            //////////////////////////////////
            {
                User tempObj = JsonConvert.DeserializeObject<User>(obj.ToJson().Replace("ISODate(", "").Replace("\")", "\"").Replace("ObjectId(", ""));
                usersFromMongo.Add(tempObj);
            }
        }

        public List<LocalUsers> getPostgreTransactions()
        {
            using (authdevContext payCon = new authdevContext())
            {
                List<LocalUsers> postgreTransactions = payCon.LocalUsers.ToList();
                return postgreTransactions;
            }
        }
    }
}
