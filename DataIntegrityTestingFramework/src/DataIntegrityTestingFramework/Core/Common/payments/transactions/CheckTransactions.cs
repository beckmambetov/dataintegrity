﻿using DataIntegrityTestingFramework.Core.Tools.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static DataIntegrityTestingFramework.Core.ConfigurationReader;
using DataIntegrityTestingFramework.payments;
using DataIntegrityTestingFramework.Core.Tools;
using DataIntegrityTestingFramework.dbcontext;

namespace DataIntegrityTestingFramework.Core.Common.payments.transactions
{
    public class CheckTransactions
    {
        public void checkTransactions()
        {
            try
            {
                List<TransactionsMongo> mongoTransactionsList = getMongoTransactionsList();
                List<Transactions> postgreTransactions = getPostgreTransactions();
                Transactions postgreT;
                int notFound = 0;
                int notEqual = 0;
                int total = 0;
                int pass = 0;
                Logger.WriteLog("------------------- TRANSACTIONS CHECK (From MongoDb to Postgre) ---------------------");
                foreach (TransactionsMongo mongoT in mongoTransactionsList)
                {
                    total++;
                    if (!postgreTransactions.Exists(s => s.MongoId == mongoT._id))
                    {
                        notFound++;
                        Logger.WriteLog("Transaction operation with mongoId: " + mongoT._id + " is absent in PostgresDB. ");
                    }
                    else
                    {
                         postgreT = postgreTransactions.Find(s => s.MongoId == mongoT._id);
                         if (!(postgreT.MongoId == mongoT._id && postgreT.ReferenceId == mongoT.reference_id && postgreT.ReferenceType == mongoT.reference_type
                            && postgreT.ReferenceReleaseNum == mongoT.reference_release_num && postgreT.UserId == mongoT.user_id))
                        {
                            notEqual++;
                            Logger.WriteLog("Some transaction data is not equals in object with mongoId: " + mongoT._id);
                        }
                        else
                        {
                            pass++;
                        }
                    }
                }
                Logger.WriteLog("------------------------ TOTAL: " + total + "PASSED: "+ pass+ " NOT FOUND: " + notFound + " NOT EQUALS: " + notEqual);
            }
            catch (Exception e)
            {
                Logger.WriteLog("EXCEPTION: " + e.Message);
            }
        }

        public void checkElasticSearchTransactions()
        {
            try
            {
                Tools.Entities.Hits allElasticTransactions = new ElasticSearchConnector().getAllTransactions();
                List<Transactions> postgreTransactions  = getPostgreTransactions();
                Tools.Entities.Hit elasticTr;
                int notFound = 0;
                int notEqual = 0;
                int total = 0;
                int pass = 0;
                Logger.WriteLog("------------------- ELASTIC SEARCH TRANSACTIONS CHECK (From Postgre to Elastic) -------------");
                foreach(Transactions tr in postgreTransactions)
                {
                    total++;
                    if (!allElasticTransactions.hits.Exists(s => s._id == tr.Id.ToString()))
                    {
                        notFound++;
                        Logger.WriteLog("Transactions operation with id: " + tr.Id + " is absent in ElasticSearch.");
                    }
                    else
                    {
                        elasticTr = allElasticTransactions.hits.Find(s => s._id == tr.Id.ToString());
                        if (!(elasticTr._id == tr.Id.ToString() && elasticTr._source.user_id == tr.UserId.ToString()
                            && elasticTr._source.transaction_id == tr.Id.ToString()))
                        {
                            notEqual++;
                            Logger.WriteLog("Some transactions data is not equals in object with id: " + tr.Id);
                        }
                        else
                        {
                            pass++;
                        }
                    }
                }
                Logger.WriteLog("------------------------ TOTAL: " + total + " PASSED: " + pass + " NOT FOUND: " + notFound + " NOT EQUALS: " + notEqual);
            }
            catch (Exception e)
            {
                Logger.WriteLog("EXCEPTION: " + e.Message);
            }
        }

        public List<TransactionsMongo> getMongoTransactionsList()
        {
            MongoConnector mg = new MongoConnector(dbConfig.mongoLMS.connectionString);

            List<TransactionsMongo> transactionsFromMongo = new List<TransactionsMongo>();
            IMongoCollection<Object> collection = mg.getCollection(mg.getDatabase(ConfigurationReader.dbConfig.mongoLMS.dbName), "transactions");
            Object bla;
           
                foreach (Object obj in collection.Find("{}").ToList())
                {
                try
                {
                    bla = obj;
                    TransactionsMongo tempObj = JsonConvert.DeserializeObject<TransactionsMongo>(obj.ToJson().Replace("ISODate(", "").Replace(")", "").Replace("ObjectId(", ""));
                    transactionsFromMongo.Add(tempObj);
                }
            catch (Exception e)
            {
                Logger.WriteLog("EXCEPTION: " + e.Message );
            }
        }
            
            return transactionsFromMongo;
        }

        public List<Transactions> getPostgreTransactions()
        {
            using (paymentsContext payCon = new paymentsContext())
            {
                List<Transactions> postgreTransactions = payCon.Transactions.ToList();
                return postgreTransactions;
            }           
        }
    }
}
