﻿using DataIntegrityTestingFramework.Core.Tools.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static DataIntegrityTestingFramework.Core.ConfigurationReader;
using DataIntegrityTestingFramework.payments;
using DataIntegrityTestingFramework.Core.Tools;

namespace DataIntegrityTestingFramework.Core.Common.payments.registrations
{
    public class CheckRegistrations
    {
        public void checkRegistrations()
        {
            try
            {
                List<RegistrationsMongo> mongoRegistrationsList = getMongoRegistrationsList();
                List<Registrations> postgreRegistrations = getPostgreRegistrations();
                Registrations postgreR;
                int notFound = 0;
                int notEqual = 0;
                int total = 0;
                int pass = 0;
                Logger.WriteLog("------------------- REGISTRATIONS CHECK (From MongoDb to Postgre) ---------------");
                foreach (RegistrationsMongo mongoR in mongoRegistrationsList)
                {
                    total++;
                    if (!postgreRegistrations.Exists(s => s.MongoId == mongoR._id))
                    {
                        notFound++;
                        Logger.WriteLog("Registration operation with mongoId: " + mongoR._id + " is absent in PostgresDB.");
                    }
                    else
                    {
                        postgreR = postgreRegistrations.Find(s => s.MongoId == mongoR._id);
                        if (!(postgreR.MongoId == mongoR._id && postgreR.ReferenceId == mongoR.reference_id
                           && postgreR.ReferenceReleaseNum == mongoR.reference_release_num && postgreR.UserId == mongoR.user_id))
                        {
                            notEqual++;
                            Logger.WriteLog("Some registration data is not equals in object with mongoId: " + mongoR._id);
                        }
                        else
                        {
                            pass++;
                        }
                    }
                }
                Logger.WriteLog("------------------------ TOTAL: " + total + " PASSED: " + pass + " NOT FOUND: " + notFound + " NOT EQUALS: " + notEqual);
            }
            catch (Exception e)
            {
                Logger.WriteLog("EXCEPTION: " + e.Message);
            }
        }

        public List<RegistrationsMongo> getMongoRegistrationsList()
        {
            MongoConnector mg = new MongoConnector(dbConfig.mongoLMS.connectionString);

            List<RegistrationsMongo> registrationsFromMongo = new List<RegistrationsMongo>();
            IMongoCollection<Object> collection = mg.getCollection(mg.getDatabase(ConfigurationReader.dbConfig.mongoLMS.dbName), "registrations");
            foreach (Object obj in collection.Find("{}").ToList())
            {
                RegistrationsMongo tempObj = JsonConvert.DeserializeObject<RegistrationsMongo>(obj.ToJson().Replace("ISODate(", "").Replace(")", "").Replace("ObjectId(", ""));
                registrationsFromMongo.Add(tempObj);
            }

            return registrationsFromMongo;
        }

        public List<Registrations> getPostgreRegistrations()
        {
            using (paymentsContext payCon = new paymentsContext())
            {
                List<Registrations> postgreRegistrations = payCon.Registrations.ToList();
                return postgreRegistrations;
            }
        }
    }
}
