﻿using DataIntegrityTestingFramework.Core.Tools;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework.Core.Common.courses
{
    public class CheckCourses
    {
        public void checkCoursesFromLMSinElastic() {

            List<Tools.Entities.Courses> coursesLMS = getMongoCoursessList();
            Hits allElasticcourses = new ElasticSearchConnector().getAllCourses();
            Tools.Logger.WriteLog("\n------------------- CHECK COURSES FROM LMS IN ELASTIC ------------------------------");

            List<Tools.Entities.Courses> coursesLMSNotTraped = new List<Tools.Entities.Courses>();
            foreach (Tools.Entities.Courses course in coursesLMS) {
                if (!course.status.Equals("tharped")) {
                    coursesLMSNotTraped.Add(course);
                }


            }




        }


        public List<Tools.Entities.Courses> getMongoCoursessList(int count = 50000)
        {
            MongoConnector mg = new MongoConnector(ConfigurationReader.dbConfig.mongoLMS.connectionString);

            List<Tools.Entities.Courses> coursesFromMongo = new List<Tools.Entities.Courses>();
            IMongoCollection<Object> collection = mg.getCollection(mg.getDatabase(ConfigurationReader.dbConfig.mongoLMS.dbName), "courses");
            foreach (Object obj in collection.Find("{}").Limit(count).ToList())
            {
                Tools.Entities.Courses tempObj = JsonConvert.DeserializeObject<Tools.Entities.Courses>(obj.ToJson().Replace("ISODate(", "").Replace(")", "").Replace("ObjectId(", ""));
                coursesFromMongo.Add(tempObj);
            }
            return coursesFromMongo;
        }
    }
}
