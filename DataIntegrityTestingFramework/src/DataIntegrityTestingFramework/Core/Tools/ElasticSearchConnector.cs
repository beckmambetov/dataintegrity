﻿using MongoDB.Bson;
using Nest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static DataIntegrityTestingFramework.Core.ConfigurationReader;

namespace DataIntegrityTestingFramework.Core.Tools
{
    public class ElasticSearchConnector
    {

        ConnectionSettings connectionSettings = new ConnectionSettings(new Uri(dbConfig.elasticSearch.connectionString));

        private ElasticClient getConnection()
        {
            return new ElasticClient(connectionSettings);

        }
        public Entities.Hits getAllTransactions(int count = 10000)
        {
            try
            {
                ISearchResponse<JObject> response = getConnection().Search<JObject>(s => s
                 .Index("transaction_with_revenues").Type("transaction_with_revenue").Size(count)
                  .Query(q => q.MatchAll()));
                return fromResponseToHitsInTransactions(response);
            }
            catch (Exception e)
            {
                Logger.WriteLog("EXCEPTION IN getAllTransactions(): " + e.Message + "\n" + e.StackTrace);
                return null;
            }

        }
        public Hits getCoursesByTitle(string title, int count = 10)
        {
            try
            {
                ISearchResponse<JObject> response = getConnection().Search<JObject>(s => s
                  .Index("courses").Type("course").Size(count)
                  .Query(q => q
          .Match(m => m.Field("title").Query(title)
          )));
                return fromResponseToHitsInCourses(response);
            }
            catch (Exception e)
            {
                Logger.WriteLog("EXCEPTION IN getCoursesByTitle(): " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }
        public Hits getAllCourses(int defaultCount = 10000)
        {
            try
            {
                ISearchResponse<JObject> response = getConnection().Search<JObject>(sd => sd
                        .Index("courses")
                        .Type("course").Size(defaultCount)
                        .Query(q => q.MatchAll()));
                return fromResponseToHitsInCourses(response);
            }
            catch (Exception e)
            {
                Logger.WriteLog("EXCEPTION IN getAllCourses(): " + e.Message + "\n" + e.StackTrace);
                return null;
            }

        }

        private Hits fromResponseToHitsInCourses(ISearchResponse<JObject> response)
        {
            try
            {
                Hits hits = new Hits();
                hits.max_score = Double.Parse(response.MaxScore.ToString());
                hits.total = Int32.Parse(response.Total.ToString());
                var sds = response.Hits;
                List<Hit> hitsToAdd = new List<Hit>();
                foreach (IHit<JObject> element in response.Hits)
                {
                    Hit hit = new Hit();
                    Source source;
                    Newtonsoft.Json.Linq.JObject temp = element.Source;
                    source = JsonConvert.DeserializeObject<Source>(temp.ToString()); //.Replace("ISODate(", "").Replace(")", "").Replace("ObjectId(", "")
                    hit._source = source;
                    hit._id = element.Id;
                    hit._index = element.Index;
                    hit._score = Double.Parse(element.Score.ToString());
                    hit._type = element.Type;
                    hitsToAdd.Add(hit);
                }
                hits.hits = hitsToAdd;
                return hits;
            }
            catch (Exception e) {
                Logger.WriteLog("EXCEPTION IN fromResponseToHitsInCourses(): " + e.Message + "\n" + e.StackTrace);
                return null;
            } 
        }

        private DataIntegrityTestingFramework.Core.Tools.Entities.Hits fromResponseToHitsInTransactions(ISearchResponse<JObject> response)
        {
            try { 
            DataIntegrityTestingFramework.Core.Tools.Entities.Hits hits = new Entities.Hits();
            hits.max_score = Double.Parse(response.MaxScore.ToString());
            hits.total = Int32.Parse(response.Total.ToString());
            List<Entities.Hit> hitsToAdd = new List<Entities.Hit>();

            foreach (IHit<JObject> element in response.Hits)
            {
                Entities.Hit hit = new Entities.Hit();
                Entities.Source source;
                Newtonsoft.Json.Linq.JObject temp = element.Source;
                source = JsonConvert.DeserializeObject<Entities.Source>(temp.ToString()); //.Replace("ISODate(", "").Replace(")", "").Replace("ObjectId(", "")
                hit._source = source;
                hit._id = element.Id;
                hit._index = element.Index;
                hit._score = Double.Parse(element.Score.ToString());
                hit._type = element.Type;
                hitsToAdd.Add(hit);
            }
            hits.hits = hitsToAdd;
            return hits;
        }
            catch (Exception e) {
                Logger.WriteLog("EXCEPTION IN fromResponseToHitsInTransactions(): " + e.Message + "\n" + e.StackTrace);
                return null;
            }
}
    }
}
