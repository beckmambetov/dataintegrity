﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework.Core.Tools.Entities
{
    public class TransactionsMongo
    {
            public String _id { get; set; }
            public String date { get; set; }
            public int user_id { get; set; }
            public String currency_id { get; set; }
            public String reference_id { get; set; }
            public String reference_type { get; set; }
            public double? total { get; set; }
            public double? total_usd { get; set; }
            public double? reference_release_num { get; set; }
    }
}
