﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework.Core.Tools.Entities
{
    public class TransactionsElasticSearchHits
    {
        public DataIntegrityTestingFramework.Core.Tools.Entities.Hits hits { get; set; }
    }

    public class Source
    {
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string user_id { get; set; }
        public string course_id { get; set; }
        public string transaction_id { get; set; }
        public object curriculum_id { get; set; }
        public double total { get; set; }
        public double revenue { get; set; }
        public string currency_id { get; set; }
        public string status { get; set; }
        public string enqueued_at { get; set; }
    }

    public class Hit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public double _score { get; set; }
        public Source _source { get; set; }
    }

    public class Hits
    {
        public int total { get; set; }
        public double max_score { get; set; }
        public List<Hit> hits { get; set; }
    }
}
