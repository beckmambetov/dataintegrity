﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework.Core.Tools.Entities
{
    public class RegistrationsMongo
    {
        public String _id { get; set; }
        public String status { get; set; }
        public String created { get; set; }
        public String updated { get; set; }
        public int user_id { get; set; }
        public String reference_id { get; set; }
        public String transactions_id { get; set; }
        public int created_by { get; set; }
        public int updated_by { get; set; }
        public double? reference_release_num { get; set; }
    }
}
