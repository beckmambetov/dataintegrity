﻿using DataIntegrityTestingFramework.Core.Tools;
using DataIntegrityTestingFramework.dbcontext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework.Tools.Entities
{
    public class UserBio
    {
        public UserBio() { }
        public String _id { get; set; }
        public String first_name { get; set; }
        public String last_name { get; set; }
        public String bio { get; set; }
        public String status { get; set; }
        public String created { get; set; }
        public String updated { get; set; }
        public String picture_url { get; set; }
        public String headline { get; set; }

        public override bool Equals(object obj)
        {
            UserBio newUserBio = (UserBio)obj;
            return (this._id == newUserBio._id &&
                first_name == newUserBio.first_name &&
                last_name == newUserBio.last_name &&
                bio == newUserBio.bio &&
                status == newUserBio.status &&
                created == newUserBio.created &&
                updated == newUserBio.updated &&
                picture_url == newUserBio.picture_url);
        }
    }


    public class User
    {
        public String _id { get; set; }
        public String current_points { get; set; }
        public String _type { get; set; }
        public String status { get; set; }
        public String created { get; set; }
        public String updated { get; set; }
        public String user_source { get; set; }
        public Boolean is_admin { get; set; }
        public UserBio user_bio { get; set; }
        public String payment_user_id { get; set; }

        public String default_card_payment_source_id { get; set; }
        public String updated_by { get; set; }

        public User ()
        {
            user_bio = new UserBio();
        }

        public LocalUsers ToLocalUser()
        {
            LocalUsers result = new LocalUsers();
            result.Id = Convert.ToInt32(_id);
            result.FirstName = user_bio.first_name;
            result.LastName = user_bio.last_name;
            result.ProfilePicture = user_bio.picture_url;
            result.Bio = user_bio.bio;
            result.Headline = user_bio.headline;
            return result;
        }

        public override bool Equals(object obj)
        {
            User newUser = (User)obj;
            if (obj.GetType().Equals(new Publisher().GetType()) || obj.GetType().Equals(new Instructor().GetType()))
            {
                return user_bio.first_name == newUser.user_bio.first_name &&
                 user_bio.last_name == newUser.user_bio.last_name;
            }
            else
            {
                return (user_bio.first_name == newUser.user_bio.first_name &&
                    user_bio.last_name == newUser.user_bio.last_name &&
                    user_bio.picture_url == newUser.user_bio.picture_url &&
                    user_bio.bio == newUser.user_bio.bio &&
                    user_bio.headline == newUser.user_bio.headline);
            }
        }
    }
}
