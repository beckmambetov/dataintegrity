﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework.Core.Tools.Entities
{
    public class Courses
    {
        public String _id { get; set; }
        public String _type { get; set; }
        public String title { get; set; }
        public double price { get; set; }
        public String cover_image_url { get; set; }
        public String created { get; set; }
        public String updated { get; set; }
        public Boolean featured { get; set; }
        public String status { get; set; }
        public String course_source { get; set; }
        public String description { get; set; }
        public String headline { get; set; }
        public String primary_category_id { get; set; }
        public String primary_subcategory_id { get; set; }
        public String currency_id { get; set; }
        public String course_link { get; set; }
        public int external_id { get; set; }
        public int version { get; set; }
        public String publisher_id { get; set; }
        public String slug { get; set; }
        public String release_id { get; set; }
        public List<Blocks> blocks { get; set; }
        public List<String> external_instructor_ids { get; set; }
    }

    public class Blocks
    {
        public String _id { get; set; }
        public double duration_sec { get; set; }
        public String description { get; set; }
        public int version { get; set; }
        public List<ChildBlocks> child_blocks { get; set; }
    }

    public class ChildBlocks
    {
        public String _id { get; set; }
        public double duration_sec { get; set; }
        public String description { get; set; }
        public List<String> activity_ids { get; set; }
    }
}
