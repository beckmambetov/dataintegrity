﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework.Core.Tools.Entities
{
    public class Publishers
    {
        public String _id { get; set; }
        public String label { get; set; }
        public int owner_id { get; set; }
        public String status { get; set; }
        public String created { get; set; }
        public String updated { get; set; }
        public int created_by { get; set; }
        public int updated_by { get; set; }
        public String display_type { get; set; }
        public String dba_name { get; set; }
    }
}
