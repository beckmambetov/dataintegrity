﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework.Core.Tools.Entities
{
    public class CourseLists
    {
        public String _id { get; set; }
        public String _type { get; set; }
        public String title { get; set; }
        public String description { get; set; }
        public double total_discounted_price { get; set; }
        public String career_profile_id { get; set; }
        public List<String> curriculum_step_ids { get; set; }
        public List<String> course_list_ids { get; set; }
        public List<String> carousel_element_ids { get; set; }
        public String official_partner_company_list_id { get; set; }
        public String hiring_company_list_id { get; set; }
        public String guaranteed_interview_company_list_id { get; set; }
        public String cover_image_url { get; set; }
        public String currency_id { get; set; }
        public String created { get; set; }
        public String updated { get; set; }
        public String status { get; set; }
        public String slug { get; set; }
        public int created_by { get; set; }
        public int updated_by { get; set; }
        public int version { get; set; }
    }
}
