﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework.Tools
{
    public abstract class EntityComparator
    {
        private Entity NewEntity;
        private Entity OldEntity;

        public bool compare()
        {
            Type type = NewEntity.GetType();
            FieldInfo[] fields = type.GetFields();
            foreach (FieldInfo x in fields)
            {
                if (x.GetValue(NewEntity) != x.GetValue(OldEntity))
                {
                    return false;
                }
            }
            return true;

        }

    }
}
