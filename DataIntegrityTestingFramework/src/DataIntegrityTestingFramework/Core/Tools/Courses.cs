﻿using DataIntegrityTestingFramework.dbcontext;
using DataIntegrityTestingFramework.Tools.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework.Core.Tools
{
    public class Courses
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public Shards _shards { get; set; }
        public Hits hits { get; set; }
    }
    public class Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }

    public class Publisher
    {
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string original_id { get; set; }

        public LocalUsers ToLocalUsers()
        {
            LocalUsers x = new LocalUsers();
            x.Id = Convert.ToInt32(original_id);
            x.FirstName = first_name.ToString();
            x.LastName = last_name.ToString();
            x.Email = email;
            return x;
        }
    }
    public class Instructor
    {
        public string email { get; set; }
        public object first_name { get; set; }
        public object last_name { get; set; }
        public string original_id { get; set; }
        public LocalUsers ToLocalUsers()
        {
            try
            {
                LocalUsers x = new LocalUsers();
                x.Id = Convert.ToInt32(original_id);
                x.FirstName = first_name.ToString();
                x.LastName = last_name.ToString();
                x.Email = email;
                return x;
            }
            catch (Exception e) {

                return null;
            }
        }
    }

    public class Source
    {
        public int buildNum { get; set; }
        // public object __invalid_name__query:queryString:options { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string original_id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public object publisher_id { get; set; }
        public object status { get; set; }
        public object release_num { get; set; }
        public object currency_id { get; set; }
        public object enqueued_at { get; set; }
        public bool? deleted { get; set; }
        public Publisher publisher { get; set; }
        public IList<Instructor> instructors { get; set; }
    }

    public class Hit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public double _score { get; set; }
        public Source _source { get; set; }
    }

    public class Hits
    {
        public int total { get; set; }
        public double max_score { get; set; }
        public List<Hit> hits { get; set; }
    }
}
