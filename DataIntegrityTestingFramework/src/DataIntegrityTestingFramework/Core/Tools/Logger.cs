﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework.Core.Tools
{
    public class Logger
    {
        //public static string logFilePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\Resuts-" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "." + "txt";
        public static string logFilePath = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin")) + "LOGS\\Resuts-" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "." + "txt";
        public static void WriteLog(string strLog)
        {
            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;



            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();

            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            log.WriteLine(strLog);
            log.Dispose();
        }

    }
}
