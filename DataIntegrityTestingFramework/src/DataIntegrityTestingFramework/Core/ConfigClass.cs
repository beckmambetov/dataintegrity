﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework
{
    public class ConfigClass
    {
        public class sourcePostgres
        {
            public string host { get; set; }
            public string userName { get; set; }
            public string password { get; set; }
            public string dbName { get; set; }
        }

       
        public class sourceMongo
        {
            public string connectionString { get; set; }
            public string collection { get; set; }
            public string dbName { get; set; }
        }
        public class paymentsPostgres
        {
            public string host { get; set; }
            public string userName { get; set; }
            public string password { get; set; }
            public string dbName { get; set; }
        }
        public class elasticSearch
        {
            public string connectionString { get; set; }

        }
        public class dbConfig
        {
            public sourcePostgres sourcePostgres { get; set; }
            public paymentsPostgres paymentsPostgres { get; set; }
            public elasticSearch elasticSearch { get; set; }
            public sourceMongo mongoLMS { get; set; }

        }
    }
}
