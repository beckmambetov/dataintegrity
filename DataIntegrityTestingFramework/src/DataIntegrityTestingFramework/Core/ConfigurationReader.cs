﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static DataIntegrityTestingFramework.ConfigClass;
using Newtonsoft.Json;

namespace DataIntegrityTestingFramework.Core
{
    public static class ConfigurationReader
    {
        public static dbConfig dbConfig;

        static ConfigurationReader()
        {
            string text = System.IO.File.ReadAllText("DbConfig.json");
            dbConfig = JsonConvert.DeserializeObject<dbConfig>(text);
        }

    }
}
