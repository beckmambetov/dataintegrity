﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static DataIntegrityTestingFramework.Core.ConfigurationReader;

namespace DataIntegrityTestingFramework
{

     public class MongoConnector
    {

        IMongoClient client;// = new MongoClient(dbConfig.sourceMongo.connectionString);

        public MongoConnector(String conn)
        {
            client = new MongoClient(conn);
        }

        public IMongoDatabase getDatabase(String dbName) {
            
            return client.GetDatabase(dbName);
        }

        public IMongoCollection<Object> getCollection(IMongoDatabase database, String colletionName) {

            return database.GetCollection<Object>(colletionName);

        }

      
    }
}
