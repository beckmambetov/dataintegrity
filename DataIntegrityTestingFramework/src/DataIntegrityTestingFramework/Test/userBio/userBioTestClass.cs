﻿using DataIntegrityTestingFramework.Core.Common.userBio;
using DataIntegrityTestingFramework.Core.Tools;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataIntegrityTestingFramework.Test.userBio
{
    [TestFixture]
    public class userBioTestClass
    {
        checkUserBio uBio = new checkUserBio();
        
        [Test]
        public void userBio() {
            try
            {
                uBio.checkUserBioWithDB();
            } catch (Exception e)
            {
                Logger.WriteLog(e.Message);
                Logger.WriteLog(e.InnerException.Message);
            }

        }

        [Test]
        public void userBioElastic()
        {
            try
            {
                uBio.checkUserFromElastic();
            }
            catch (Exception e)
            {
                Logger.WriteLog(e.Message);
                Logger.WriteLog(e.InnerException.Message);
            }

        }
    }
}
