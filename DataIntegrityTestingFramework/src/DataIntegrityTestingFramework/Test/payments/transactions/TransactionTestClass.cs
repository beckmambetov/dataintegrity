﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using DataIntegrityTestingFramework.Core.Common.payments.transactions;
using DataIntegrityTestingFramework.Core.Tools;

namespace DataIntegrityTestingFramework.Test.payments.currencies
{
    [TestFixture]
    public class CurrenciesTestClass
    {
        CheckTransactions checkTransactions = new CheckTransactions();

        [Test]
        public void checkTransactionsTest()
        {
           checkTransactions.checkTransactions();
        }

        [Test]
        public void checkElasticSearchTransactions()
        {
            checkTransactions.checkElasticSearchTransactions();
        }
    }
}
