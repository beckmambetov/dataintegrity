﻿using System;
using System.Collections.Generic;

namespace DataIntegrityTestingFramework.dbcontext
{
    public partial class MessageBackup
    {
        public int Id { get; set; }
        public string ErrorMessage { get; set; }
        public string Exchange { get; set; }
        public string Message { get; set; }
        public string MessageId { get; set; }
        public string QueueName { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
