﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using DataIntegrityTestingFramework.Core;

namespace DataIntegrityTestingFramework.dbcontext
{
    public partial class authdevContext : DbContext
    {
        public virtual DbSet<AiccDomains> AiccDomains { get; set; }
        public virtual DbSet<ApiClaims> ApiClaims { get; set; }
        public virtual DbSet<ApiResources> ApiResources { get; set; }
        public virtual DbSet<ApiScopeClaims> ApiScopeClaims { get; set; }
        public virtual DbSet<ApiScopes> ApiScopes { get; set; }
        public virtual DbSet<ApiSecrets> ApiSecrets { get; set; }
        public virtual DbSet<Claims> Claims { get; set; }
        public virtual DbSet<ClientClaims> ClientClaims { get; set; }
        public virtual DbSet<ClientCorsOrigins> ClientCorsOrigins { get; set; }
        public virtual DbSet<ClientGrantTypes> ClientGrantTypes { get; set; }
        public virtual DbSet<ClientIdPrestrictions> ClientIdPrestrictions { get; set; }
        public virtual DbSet<ClientPostLogoutRedirectUris> ClientPostLogoutRedirectUris { get; set; }
        public virtual DbSet<ClientRedirectUris> ClientRedirectUris { get; set; }
        public virtual DbSet<ClientScopes> ClientScopes { get; set; }
        public virtual DbSet<ClientSecrets> ClientSecrets { get; set; }
        public virtual DbSet<Clients> Clients { get; set; }
        public virtual DbSet<ExternalUsers> ExternalUsers { get; set; }
        public virtual DbSet<IdentityClaims> IdentityClaims { get; set; }
        public virtual DbSet<IdentityResources> IdentityResources { get; set; }
        public virtual DbSet<LocalUserClaims> LocalUserClaims { get; set; }
        public virtual DbSet<LocalUsers> LocalUsers { get; set; }
        public virtual DbSet<MessageBackup> MessageBackup { get; set; }
        public virtual DbSet<PersistedGrants> PersistedGrants { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
            optionsBuilder.UseNpgsql(@"Port=5432;Host="+ ConfigurationReader.dbConfig.sourcePostgres.host + ";Database="+ ConfigurationReader.dbConfig.sourcePostgres.dbName + ";Username="+ ConfigurationReader.dbConfig.sourcePostgres.userName+ ";Password="+ ConfigurationReader.dbConfig.sourcePostgres.password + "");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApiClaims>(entity =>
            {
                entity.HasIndex(e => e.ApiResourceId)
                    .HasName("IX_ApiClaims_ApiResourceId");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.HasOne(d => d.ApiResource)
                    .WithMany(p => p.ApiClaims)
                    .HasForeignKey(d => d.ApiResourceId);
            });

            modelBuilder.Entity<ApiResources>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("IX_ApiResources_Name")
                    .IsUnique();

                entity.Property(e => e.Description)
                    .HasColumnType("varchar")
                    .HasMaxLength(1000);

                entity.Property(e => e.DisplayName)
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<ApiScopeClaims>(entity =>
            {
                entity.HasIndex(e => e.ApiScopeId)
                    .HasName("IX_ApiScopeClaims_ApiScopeId");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.HasOne(d => d.ApiScope)
                    .WithMany(p => p.ApiScopeClaims)
                    .HasForeignKey(d => d.ApiScopeId);
            });

            modelBuilder.Entity<ApiScopes>(entity =>
            {
                entity.HasIndex(e => e.ApiResourceId)
                    .HasName("IX_ApiScopes_ApiResourceId");

                entity.HasIndex(e => e.Name)
                    .HasName("IX_ApiScopes_Name")
                    .IsUnique();

                entity.Property(e => e.Description)
                    .HasColumnType("varchar")
                    .HasMaxLength(1000);

                entity.Property(e => e.DisplayName)
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.HasOne(d => d.ApiResource)
                    .WithMany(p => p.ApiScopes)
                    .HasForeignKey(d => d.ApiResourceId);
            });

            modelBuilder.Entity<ApiSecrets>(entity =>
            {
                entity.HasIndex(e => e.ApiResourceId)
                    .HasName("IX_ApiSecrets_ApiResourceId");

                entity.Property(e => e.Description)
                    .HasColumnType("varchar")
                    .HasMaxLength(1000);

                entity.Property(e => e.Type)
                    .HasColumnType("varchar")
                    .HasMaxLength(250);

                entity.Property(e => e.Value)
                    .HasColumnType("varchar")
                    .HasMaxLength(2000);

                entity.HasOne(d => d.ApiResource)
                    .WithMany(p => p.ApiSecrets)
                    .HasForeignKey(d => d.ApiResourceId);
            });

            modelBuilder.Entity<ClientClaims>(entity =>
            {
                entity.HasIndex(e => e.ClientId)
                    .HasName("IX_ClientClaims_ClientId");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(250);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(250);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientClaims)
                    .HasForeignKey(d => d.ClientId);
            });

            modelBuilder.Entity<ClientCorsOrigins>(entity =>
            {
                entity.HasIndex(e => e.ClientId)
                    .HasName("IX_ClientCorsOrigins_ClientId");

                entity.Property(e => e.Origin)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(150);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientCorsOrigins)
                    .HasForeignKey(d => d.ClientId);
            });

            modelBuilder.Entity<ClientGrantTypes>(entity =>
            {
                entity.HasIndex(e => e.ClientId)
                    .HasName("IX_ClientGrantTypes_ClientId");

                entity.Property(e => e.GrantType)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(250);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientGrantTypes)
                    .HasForeignKey(d => d.ClientId);
            });

            modelBuilder.Entity<ClientIdPrestrictions>(entity =>
            {
                entity.ToTable("ClientIdPRestrictions");

                entity.HasIndex(e => e.ClientId)
                    .HasName("IX_ClientIdPRestrictions_ClientId");

                entity.Property(e => e.Provider)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientIdPrestrictions)
                    .HasForeignKey(d => d.ClientId);
            });

            modelBuilder.Entity<ClientPostLogoutRedirectUris>(entity =>
            {
                entity.HasIndex(e => e.ClientId)
                    .HasName("IX_ClientPostLogoutRedirectUris_ClientId");

                entity.Property(e => e.PostLogoutRedirectUri)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(2000);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientPostLogoutRedirectUris)
                    .HasForeignKey(d => d.ClientId);
            });

            modelBuilder.Entity<ClientRedirectUris>(entity =>
            {
                entity.HasIndex(e => e.ClientId)
                    .HasName("IX_ClientRedirectUris_ClientId");

                entity.Property(e => e.RedirectUri)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(2000);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientRedirectUris)
                    .HasForeignKey(d => d.ClientId);
            });

            modelBuilder.Entity<ClientScopes>(entity =>
            {
                entity.HasIndex(e => e.ClientId)
                    .HasName("IX_ClientScopes_ClientId");

                entity.Property(e => e.Scope)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientScopes)
                    .HasForeignKey(d => d.ClientId);
            });

            modelBuilder.Entity<ClientSecrets>(entity =>
            {
                entity.HasIndex(e => e.ClientId)
                    .HasName("IX_ClientSecrets_ClientId");

                entity.Property(e => e.Description)
                    .HasColumnType("varchar")
                    .HasMaxLength(2000);

                entity.Property(e => e.Type)
                    .HasColumnType("varchar")
                    .HasMaxLength(250);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(2000);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientSecrets)
                    .HasForeignKey(d => d.ClientId);
            });

            modelBuilder.Entity<Clients>(entity =>
            {
                entity.HasIndex(e => e.ClientId)
                    .HasName("IX_Clients_ClientId")
                    .IsUnique();

                entity.Property(e => e.ClientId)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.Property(e => e.ClientName)
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.Property(e => e.ClientUri)
                    .HasColumnType("varchar")
                    .HasMaxLength(2000);

                entity.Property(e => e.ProtocolType)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<ExternalUsers>(entity =>
            {
                entity.HasIndex(e => e.UserId)
                    .HasName("IX_ExternalUsers_UserId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ExternalUsers)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<IdentityClaims>(entity =>
            {
                entity.HasIndex(e => e.IdentityResourceId)
                    .HasName("IX_IdentityClaims_IdentityResourceId");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.HasOne(d => d.IdentityResource)
                    .WithMany(p => p.IdentityClaims)
                    .HasForeignKey(d => d.IdentityResourceId);
            });

            modelBuilder.Entity<IdentityResources>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("IX_IdentityResources_Name")
                    .IsUnique();

                entity.Property(e => e.Description)
                    .HasColumnType("varchar")
                    .HasMaxLength(1000);

                entity.Property(e => e.DisplayName)
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<LocalUserClaims>(entity =>
            {
                entity.HasKey(e => new { e.LocalUserId, e.ClaimId })
                    .HasName("PK_LocalUserClaims");

                entity.HasIndex(e => e.ClaimId)
                    .HasName("IX_LocalUserClaims_ClaimId");

                entity.HasOne(d => d.Claim)
                    .WithMany(p => p.LocalUserClaims)
                    .HasForeignKey(d => d.ClaimId);

                entity.HasOne(d => d.LocalUser)
                    .WithMany(p => p.LocalUserClaims)
                    .HasForeignKey(d => d.LocalUserId);
            });

            modelBuilder.Entity<LocalUsers>(entity =>
            {
                entity.HasIndex(e => e.Email)
                    .HasName("IX_LocalUsers_Email")
                    .IsUnique();

                entity.Property(e => e.Email).IsRequired();
            });

            modelBuilder.Entity<PersistedGrants>(entity =>
            {
                entity.HasKey(e => new { e.Key, e.Type })
                    .HasName("PK_PersistedGrants");

                entity.HasIndex(e => e.SubjectId)
                    .HasName("IX_PersistedGrants_SubjectId");

                entity.HasIndex(e => new { e.SubjectId, e.ClientId })
                    .HasName("IX_PersistedGrants_SubjectId_ClientId");

                entity.HasIndex(e => new { e.SubjectId, e.ClientId, e.Type })
                    .HasName("IX_PersistedGrants_SubjectId_ClientId_Type");

                entity.Property(e => e.Key)
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.Property(e => e.Type)
                    .HasColumnType("varchar")
                    .HasMaxLength(50);

                entity.Property(e => e.ClientId)
                    .IsRequired()
                    .HasColumnType("varchar")
                    .HasMaxLength(200);

                entity.Property(e => e.Data).IsRequired();

                entity.Property(e => e.SubjectId)
                    .HasColumnType("varchar")
                    .HasMaxLength(200);
            });
        }
    }
}