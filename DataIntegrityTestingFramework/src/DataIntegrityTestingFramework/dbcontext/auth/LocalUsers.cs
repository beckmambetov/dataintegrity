﻿using DataIntegrityTestingFramework.Core.Tools;
using DataIntegrityTestingFramework.Tools.Entities;
using System;
using System.Collections.Generic;

namespace DataIntegrityTestingFramework.dbcontext
{
    public partial class LocalUsers
    {
        public LocalUsers()
        {
            ExternalUsers = new HashSet<ExternalUsers>();
            LocalUserClaims = new HashSet<LocalUserClaims>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public bool IsActive { get; set; }
        public bool IsEmailVerified { get; set; }
        public DateTime LastLoginDateTime { get; set; }
        public string LastName { get; set; }
        public bool LocalLoginAllowed { get; set; }
        public int LoginCount { get; set; }
        public string PasswordHash { get; set; }
        public string ProfilePicture { get; set; }
        public DateTime SignUpDateTime { get; set; }
        public string VerificationCode { get; set; }
        public string Bio { get; set; }
        public string Headline { get; set; }

        public virtual ICollection<ExternalUsers> ExternalUsers { get; set; }
        public virtual ICollection<LocalUserClaims> LocalUserClaims { get; set; }      

        public User ToUser()
        {
            User result = new User();
            result._id = Id.ToString();
            result.user_bio.first_name = FirstName == null ? null : FirstName;
            result.user_bio.last_name = LastName == null ? null : LastName;
            result.user_bio.picture_url = ProfilePicture == null ? null : ProfilePicture;
            result.user_bio.bio = Bio == null ? null : Bio;
            result.user_bio.headline = Headline == null ? null : Headline;
            return result;
        }

        public override bool Equals(object obj)
        {
            LocalUsers newUser = (LocalUsers)obj;
            if (obj.GetType().Equals(new Publisher().GetType()) || obj.GetType().Equals(new Instructor().GetType()))
            {
                return FirstName == newUser.FirstName &&
                LastName == newUser.LastName &&
                Id == newUser.Id &&
                Email == newUser.Email;

            } else
            {
                return false;
            }
            
            
        }
    }
}
