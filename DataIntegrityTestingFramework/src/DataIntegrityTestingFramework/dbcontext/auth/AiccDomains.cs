﻿using System;
using System.Collections.Generic;

namespace DataIntegrityTestingFramework.dbcontext
{
    public partial class AiccDomains
    {
        public int Id { get; set; }
        public DateTime? Created { get; set; }
        public string Domain { get; set; }
        public bool IsEnabled { get; set; }
        public string Purpose { get; set; }
        public DateTime? Updated { get; set; }
    }
}
