﻿using System;
using System.Collections.Generic;

namespace DataIntegrityTestingFramework.dbcontext
{
    public partial class LocalUserClaims
    {
        public int LocalUserId { get; set; }
        public int ClaimId { get; set; }

        public virtual Claims Claim { get; set; }
        public virtual LocalUsers LocalUser { get; set; }
    }
}
