﻿using System;
using System.Collections.Generic;

namespace DataIntegrityTestingFramework.dbcontext
{
    public partial class Claims
    {
        public Claims()
        {
            LocalUserClaims = new HashSet<LocalUserClaims>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }

        public virtual ICollection<LocalUserClaims> LocalUserClaims { get; set; }
    }
}
