﻿using System;
using System.Collections.Generic;

namespace DataIntegrityTestingFramework.dbcontext
{
    public partial class ExternalUsers
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public DateTime LastLoginDateTime { get; set; }
        public string LastName { get; set; }
        public int LoginCount { get; set; }
        public string NameIdentifier { get; set; }
        public string ProfilePicture { get; set; }
        public string Provider { get; set; }
        public DateTime SignUpDateTime { get; set; }
        public int? UserId { get; set; }

        public virtual LocalUsers User { get; set; }
    }
}
