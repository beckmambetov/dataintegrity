﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using DataIntegrityTestingFramework.Core;

namespace DataIntegrityTestingFramework.payments
{
    public partial class paymentsContext : DbContext
    {
        public virtual DbSet<ArInternalMetadata> ArInternalMetadata { get; set; }
        public virtual DbSet<Currencies> Currencies { get; set; }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<FailedRabbitMessages> FailedRabbitMessages { get; set; }
        public virtual DbSet<PaymentSources> PaymentSources { get; set; }
        public virtual DbSet<Registrations> Registrations { get; set; }
        public virtual DbSet<SchemaMigrations> SchemaMigrations { get; set; }
        public virtual DbSet<Transactions> Transactions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
            optionsBuilder.UseNpgsql(@"Port=5432;Host="+ ConfigurationReader.dbConfig.paymentsPostgres.host + ";Database="+ ConfigurationReader.dbConfig.paymentsPostgres.dbName + ";Username="+ ConfigurationReader.dbConfig.paymentsPostgres.userName + ";Password="+ ConfigurationReader.dbConfig.paymentsPostgres.password + "");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ArInternalMetadata>(entity =>
            {
                entity.HasKey(e => e.Key)
                    .HasName("PK_ar_internal_metadata");

                entity.ToTable("ar_internal_metadata");

                entity.Property(e => e.Key)
                    .HasColumnName("key")
                    .HasColumnType("varchar");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("varchar");
            });

            modelBuilder.Entity<Currencies>(entity =>
            {
                entity.ToTable("currencies");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.Label)
                    .HasColumnName("label")
                    .HasColumnType("varchar");

                entity.Property(e => e.MongoId)
                    .HasColumnName("mongo_id")
                    .HasColumnType("varchar");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("varchar");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.Property(e => e.UpdatedBy).HasColumnName("updated_by");
            });

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.ToTable("customers");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.PaymentUserId)
                    .HasColumnName("payment_user_id")
                    .HasColumnType("varchar");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            modelBuilder.Entity<FailedRabbitMessages>(entity =>
            {
                entity.ToTable("failed_rabbit_messages");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AppId)
                    .HasColumnName("app_id")
                    .HasColumnType("varchar");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.ErrorMessage)
                    .HasColumnName("error_message")
                    .HasColumnType("varchar");

                entity.Property(e => e.MessageId)
                    .HasColumnName("message_id")
                    .HasColumnType("varchar");

                entity.Property(e => e.RabbitMessage)
                    .HasColumnName("rabbit_message")
                    .HasColumnType("varchar");

                entity.Property(e => e.RoutingKey)
                    .HasColumnName("routing_key")
                    .HasColumnType("varchar");

                entity.Property(e => e.Timestamp).HasColumnName("timestamp");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");
            });

            modelBuilder.Entity<PaymentSources>(entity =>
            {
                entity.ToTable("payment_sources");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddressCity)
                    .HasColumnName("address_city")
                    .HasColumnType("varchar");

                entity.Property(e => e.AddressCountry)
                    .HasColumnName("address_country")
                    .HasColumnType("varchar");

                entity.Property(e => e.AddressLine1)
                    .HasColumnName("address_line1")
                    .HasColumnType("varchar");

                entity.Property(e => e.AddressLine2)
                    .HasColumnName("address_line2")
                    .HasColumnType("varchar");

                entity.Property(e => e.AddressState)
                    .HasColumnName("address_state")
                    .HasColumnType("varchar");

                entity.Property(e => e.AddressZip)
                    .HasColumnName("address_zip")
                    .HasColumnType("varchar");

                entity.Property(e => e.Brand)
                    .HasColumnName("brand")
                    .HasColumnType("varchar");

                entity.Property(e => e.CardNumberLast4)
                    .HasColumnName("card_number_last4")
                    .HasColumnType("varchar");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.ExpMonth).HasColumnName("exp_month");

                entity.Property(e => e.ExpYear).HasColumnName("exp_year");

                entity.Property(e => e.Fingerprint)
                    .HasColumnName("fingerprint")
                    .HasColumnType("varchar");

                entity.Property(e => e.GatewayCardId)
                    .HasColumnName("gateway_card_id")
                    .HasColumnType("varchar");

                entity.Property(e => e.MongoId)
                    .HasColumnName("mongo_id")
                    .HasColumnType("varchar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            modelBuilder.Entity<Registrations>(entity =>
            {
                entity.ToTable("registrations");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.MongoId)
                    .HasColumnName("mongo_id")
                    .HasColumnType("varchar");

                entity.Property(e => e.ReferenceId)
                    .HasColumnName("reference_id")
                    .HasColumnType("varchar");

                entity.Property(e => e.ReferenceReleaseNum).HasColumnName("reference_release_num");

                entity.Property(e => e.ReferenceType)
                    .HasColumnName("reference_type")
                    .HasColumnType("varchar");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("varchar");

                entity.Property(e => e.TransactionId).HasColumnName("transaction_id");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.Property(e => e.UpdatedBy).HasColumnName("updated_by");

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            modelBuilder.Entity<SchemaMigrations>(entity =>
            {
                entity.HasKey(e => e.Version)
                    .HasName("PK_schema_migrations");

                entity.ToTable("schema_migrations");

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasColumnType("varchar");
            });

            modelBuilder.Entity<Transactions>(entity =>
            {
                entity.ToTable("transactions");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.CurrencyId).HasColumnName("currency_id");

                entity.Property(e => e.Details)
                    .HasColumnName("details")
                    .HasColumnType("jsonb")
                    .HasDefaultValueSql("'\"{}\"'::jsonb");

                entity.Property(e => e.MongoId)
                    .HasColumnName("mongo_id")
                    .HasColumnType("varchar");

                entity.Property(e => e.PaymentSourceId).HasColumnName("payment_source_id");

                entity.Property(e => e.ReferenceId)
                    .HasColumnName("reference_id")
                    .HasColumnType("varchar");

                entity.Property(e => e.ReferenceReleaseNum).HasColumnName("reference_release_num");

                entity.Property(e => e.ReferenceType)
                    .HasColumnName("reference_type")
                    .HasColumnType("varchar");

                entity.Property(e => e.Total).HasColumnName("total");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });
        }
    }
}