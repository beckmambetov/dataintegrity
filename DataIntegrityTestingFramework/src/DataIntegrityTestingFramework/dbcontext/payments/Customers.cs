﻿using System;
using System.Collections.Generic;

namespace DataIntegrityTestingFramework.payments
{
    public partial class Customers
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string PaymentUserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
