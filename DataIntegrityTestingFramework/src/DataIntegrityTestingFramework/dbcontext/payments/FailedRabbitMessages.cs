﻿using System;
using System.Collections.Generic;

namespace DataIntegrityTestingFramework.payments
{
    public partial class FailedRabbitMessages
    {
        public int Id { get; set; }
        public string RabbitMessage { get; set; }
        public string RoutingKey { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string MessageId { get; set; }
        public string AppId { get; set; }
        public DateTime? Timestamp { get; set; }
    }
}
