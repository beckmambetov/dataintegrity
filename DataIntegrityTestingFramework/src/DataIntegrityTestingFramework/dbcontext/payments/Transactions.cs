﻿using System;
using System.Collections.Generic;

namespace DataIntegrityTestingFramework.payments
{
    public partial class Transactions
    {
        public int Id { get; set; }
        public string MongoId { get; set; }
        public int? UserId { get; set; }
        public string ReferenceType { get; set; }
        public string ReferenceId { get; set; }
        public int? ReferenceReleaseNum { get; set; }
        public int? CurrencyId { get; set; }
        public double? Total { get; set; }
        public int? PaymentSourceId { get; set; }
        public string Details { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
