﻿using System;
using System.Collections.Generic;

namespace DataIntegrityTestingFramework.payments
{
    public partial class PaymentSources
    {
        public int Id { get; set; }
        public string MongoId { get; set; }
        public string GatewayCardId { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressZip { get; set; }
        public string AddressCountry { get; set; }
        public string Brand { get; set; }
        public string CardNumberLast4 { get; set; }
        public int? ExpMonth { get; set; }
        public int? ExpYear { get; set; }
        public string Fingerprint { get; set; }
        public int? UserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
