﻿using System;
using System.Collections.Generic;

namespace DataIntegrityTestingFramework.payments
{
    public partial class Registrations
    {
        public int Id { get; set; }
        public string MongoId { get; set; }
        public int? UserId { get; set; }
        public string ReferenceType { get; set; }
        public string ReferenceId { get; set; }
        public int? ReferenceReleaseNum { get; set; }
        public int? TransactionId { get; set; }
        public string Status { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
